#!/bin/sh

# check output image 
if lsblk -f -o FSTYPE /dev/sdb | grep -q 'ext4' &> /dev/null; then
    echo "image already formated"
else
    echo "image not formatted, formatting..."
    mkfs.ext4 -L output /dev/sdb
fi